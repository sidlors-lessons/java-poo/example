public abstract class Animal {

    protected String familia;
    double peso;

    public void comer(){
        System.out.println("estoy comiendo, yumi yumi");
    }

    public abstract void hazEjercicio();
    
    public abstract void hazCazar();

    public String getFamilia() {
        return familia;
    }
}
