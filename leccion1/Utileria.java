import java.util.Random;

public class Utileria {

    private Utileria() {
    }

    public static Animal regalameRandomMascota() {
        Animal animalito = null;
        int caseInt = randomInteger(3);

        switch (caseInt) {
        case 0:
            animalito = new Hamster();
            break;
        case 1:
            animalito = new Lagartija();
            break;
        case 2:
            animalito = new Perro();
            break;
        default:
            break;
        }

        return animalito;
    }

    private static int randomInteger(int max) {
        return (int) (Math.random() * max);
    }

}
