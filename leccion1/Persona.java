public class Persona {

    private int edad;
    String nombre;
    String[] medioContactos;

    // Getter y Setter

    // lectura
    public int getEdad() {
        return edad;
    }

    // escritura
    public void setEdad(int edad) {
        if (edad < 0) {
            System.err.println("Error edad no valida: " + edad);
        } else {
            this.edad = edad;
        }
    }
}
