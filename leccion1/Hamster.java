public class Hamster extends Animal{


    public Hamster(){
        familia="ROEDOR";
    }

    @Override
    public void hazEjercicio() {
        System.out.println("Me subo a la rueda");
    }

    @Override
    public void hazCazar() {
        comer();
    }
    
}
