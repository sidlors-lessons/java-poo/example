public class Perro extends Animal{

    public Perro(){
        familia="CANINO";
    }

    @Override
    public void hazEjercicio() {
        System.out.println("Correa y mi dueño dueño: a correr!!");
    }

    @Override
    public void hazCazar() {
        comer();
    }
    
}
