public class Lagartija extends Animal{

    public Lagartija(){
        familia="REPTIL";
    }

    @Override
    public void hazEjercicio() {
        System.out.println("Hare lagartijaz en la pared");
    }

    @Override
    public void hazCazar() {
       System.out.println("Buscando insectos....");
        
    }
    
}
